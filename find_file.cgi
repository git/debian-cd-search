#!/usr/bin/perl
#
# find_file.cgi
#
# Look through a provided database to find which CD/DVD image(s)
# contain a specified Debian package or source file.
# 
# Copyright (c) 2011-2024 Steve McIntyre <93sam@debian.org>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA

use strict;
use warnings;
use threads;
use DB_File;
use CGI;
use ConfigReader::Simple;
use DBI;
use DBD::SQLite;

my %conf;
my $cdimage_url = "http://cdimage.debian.org/cdimage/";
my $source_url = "https://git.einval.com/cgi-bin/gitweb.cgi?p=debian-cd-search.git";
my @AREAS;
my %num_files;
my %fileinfo;
my %imageinfo;
my $query_term;
my $query_type;
my %results;
my @chosen_areas;
my $l = "";
my $max_count = 1000;
my $header1 = "";
my $header2 = "";
my $footer1 = "";
my $footer2 = "";
my $last_update = 0;

my $version = "0.12";
my $title_base = "Debian CD search engine";

my $q = new CGI;
my $mode = "none";
my $authorname = "Steve McIntyre";
my $authormail = '93sam@debian.org';

sub set_default_config () {
    $conf{'dbdir'} = "/home/steve/debian/debian-cd/search/search-db";
    $conf{'htmldir'} = "/home/steve/debian/debian-cd/html";
    $conf{'debug'} = 0;
    $conf{'dbtype'} = "sqlite";
}

# If we can find an appropriately-name config file, read it and
# over-write the default config for $conf{'dbdir'} and $conf{'htmldir'}
sub read_config () {
    my $config_file;

    $config_file = $0;
    $config_file =~ s/find_file.cgi/find_file.cfg/;
    if (-r $config_file) {
        my $config = ConfigReader::Simple->new($config_file);
        foreach my $key (keys %conf) {
            if ($config->exists($key)) {
                $conf{$key} = $config->get($key);
            }
        }
    }
}

sub file_mtime {
    my ($file) = shift;
    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
        $atime,$mtime,$ctime,$blksize,$blocks)
        = lstat($file);
    if (! $dev) {
        return -$!;
    }
    return $mtime;
}

sub read_files ($) {
    my $lang = shift;
    
    open IN, "<", "$conf{'htmldir'}/header1.$lang.html";
    while(<IN>) { $header1 .= $_; }
    close(IN);
    open IN, "<", "$conf{'htmldir'}/header2.$lang.html";
    while(<IN>) { $header2 .= $_; }
    close(IN);
    open IN, "<", "$conf{'htmldir'}/footer1.$lang.html";
    while(<IN>) { $footer1 .= $_; }
    close(IN);
    open IN, "<", "$conf{'htmldir'}/footer2.$lang.html";
    while(<IN>) { $footer2 .= $_; }
    close(IN);
}

sub date_string ($) {
    my $time = shift;
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
        gmtime($time);
    $year += 1900;
    $mon += 1;
    return sprintf("%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d UTC", $year, $mon, $mday, $hour, $min, $sec);
}

sub print_header () {
    print $q->header;
}

sub print_html_header ($) {
    my $title = shift;
    print $q->start_html(
        -title=>"$title",
        -author=>"$authormail",
        -style=>{'src'=>'https://www.debian.org/debian.css'},
        );
    print $header1;
    print '<p id="breadcrumbs">cdimage-search.debian.org</p>';
    print $header2;
    print '<div id="maincol">';
}

sub print_config_if_debug () {
    if ($conf{'debug'}) {
        print $q->h2("CONFIG DEBUG"), "\n<ul>\n";
        foreach my $key (keys %conf) {
            print $q->li("config.$key: $conf{$key}");
        }
        print "</ul>\n";
    }
}

sub print_footer () {
    my $date = date_string($last_update);
    print $footer1;
    print
        $q->address("Database last updated $date\n"),
        $q->address("$title_base version $version, source at <a href=\"$source_url\">$source_url</a>\n"),
        $q->address("$authorname &lt;$authormail&gt;\n"),
        $q->hr;
    print $footer2;
    print '</div> <!-- end footer -->';
}

# Borrowed from Ikiwiki.pm
sub glob2re ($) {
    my $re=quotemeta(shift);
    $re=~s/\\\*/.*/g;
    $re=~s/\\\?/./g;
    return qr/^$re$/i;
}

sub read_text ($) {
    my $filename = shift;
    my $text = "no description";
    if (-f $filename) {
        open INFILE, "<", "$filename" || return $text;
        $text = "";
        while (<INFILE>) {
            chomp;
            $text .= $_;
        }
        close INFILE;
    }
    return $text;
}

sub log_error ($$) {
    my $errornum = shift;
    my $errortext = shift;

    print_header();
    print_html_header("$title_base");
    print
        $q->h1($title_base),
        $q->p("Error: $errortext"),
        $q->p("<a href=\"" . $q->url . "\">Search again.</a>");
    print_config_if_debug();
    print_footer();
    print $q->end_html;
    exit 0;
}

sub blank_form ($) {
    my $error = shift;

    my %area_labels;
    my %type_labels;

    foreach my $area(@AREAS) {
        $area_labels{$area} = "$area (" . read_text("$area.text") . ")";
    }
    $type_labels{"simple"} = "simple substring search";
    $type_labels{"exact"} = "exact filename search (faster); shell globs (*,?) are permitted";

    print_header();
    print_html_header("$title_base");
    $q->autoEscape(undef);
    print $q->h1("$title_base"), "\n";
    print_config_if_debug();
    print $q->p("This tool searches for files contained in Debian CD/DVD images, such as:"), "\n";
    print
        "<ul>\n",
        $q->li("package files (.deb, .udeb)"), "\n",
        $q->li("source files (.tar.gz, .tar.bz2, .diff.gz, etc.)"), "\n",
        "</ul>\n";        
    print $q->start_form(-method=>"GET");
    print $q->p("Select which set(s) of images you wish to look in:"), "\n";
    print $q->checkbox_group(-name=>'search_area',
                             -values=>\@AREAS,
                             -defaults=>['release'],
                             -linebreak=>'true',
                             -labels=>\%area_labels);
    print $q->p("And a search type: ");
    print $q->radio_group(-name=>'type',
                          -values=>['simple', 'exact'],
                          -default=>'simple',
                          -linebreak=>'true',
                          -labels=>\%type_labels);
    print $q->p("Exact lookups with no globbing will give the fastest results.");

    print $q->textfield(-name=>'query',
                        -value=>'',
                        -size=>50,
                        -maxlength=>100);
    print $q->submit(-name=>'Search',
                     value=>'Search');
    if (length($error)) {
        print $q->p({-style=>'color: red'}, "ERROR: $error"), "\n";
    }
    print $q->end_form;
    print_footer();
    print $q->end_html;
    exit 0;
}

sub list_link ($) {
    my $link = $cdimage_url . shift;
    return $link;
}

sub iso_link ($) {
    my $link = $cdimage_url . shift;
    $link =~ s/list-/iso-/g;
    $link =~ s/list\.gz$/iso/g;
    return $link;
}

sub jigdo_link ($) {
    my $link = $cdimage_url . shift;
    $link =~ s/list-/jigdo-/g;
    $link =~ s/list\.gz$/jigdo/g;
    return $link;
}

sub pretty_name ($) {
    my $name = shift;
    $name =~ s/^.*\///g;
    $name =~ s/\.list\.gz$//g;
    return $name;
}    

sub keepalive_thread () {
    $| = 1;
    $SIG{'KILL'} = sub { threads->exit(); };
    while(1) {
	sleep 10;
	print "<!-- processing, please wait... -->\n";
    }
}

set_default_config();
read_config();
read_files("en");
chdir($conf{'dbdir'}) || log_error(500, "Failed to cd to $conf{'dbdir'}: $!\n");
opendir(my $dh, ".") || log_error(500, "Failed to open $conf{'dbdir'}: $!\n");


while (defined($_ = readdir($dh))) {
    if ($conf{'dbtype'} eq "hashdb") {
	if (m/(.*)\.db$/) {
	    my $mtime = file_mtime("$1.db");
	    if ($mtime > $last_update) {
		$last_update = $mtime;
	    }
	    push(@AREAS, "$1");
	}
    }
    else {
	if (m/(.*)\.db.sqlite$/) {
	    my $mtime = file_mtime("$1.db.sqlite");
	    if ($mtime > $last_update) {
		$last_update = $mtime;
	    }
	    push(@AREAS, "$1");
	}
    }
}
closedir($dh);

@chosen_areas = $q->multi_param('search_area');
$query_term = $q->param('query');
$query_type = $q->param('type');

# Check what we've been given, if anything
if ( (!@chosen_areas) && 
     (!defined($query_term) || length($query_term) == 0)) {
    blank_form("");
}

if (defined($query_term)) {
    if ($query_term =~ m/[\@\~\[\]\{\}|#\%\<\>\'\";\\\/]/) {
	$q->param(-name=>'query', -value=>'');
	$q->param(-name=>'type', -value=>'');
	$q->param(-name=>'search_area', -value=>());
	blank_form("Invalid query string");
    }
}

if (defined($query_type)) {
    if (!($query_type eq "exact" or $query_type eq "simple")) {
	$q->param(-name=>'query', -value=>'');
	$q->param(-name=>'type', -value=>'');
	$q->param(-name=>'search_area', -value=>());
	blank_form("Invalid query string");
    }
}

if (!(@chosen_areas) && defined($query_term)) {
    blank_form("No search areas chosen");
}

foreach my $area (@chosen_areas) {
    if (not grep (/$area/, @AREAS)) {
	blank_form("Invalid search area");
    }
}

if (@chosen_areas && 
    (!defined($query_term) || length($query_term) == 0)) {
    blank_form("No search terms entered");
}

my $count = 0;
my $count_images = 0;
my $re_search;
my $using_glob = "";
if ($query_type eq "simple") {
    $re_search = glob2re('*' . join('*', split(/ /, $query_term)) . '*');
} else {
    $re_search = glob2re($query_term);
}

# If we get here, we have stuff to work with. Yay!

select STDERR; $| = 1;  # make unbuffered
select STDOUT; $| = 1;  # make unbuffered

my $start_time = time();
print_header();
print_html_header("$title_base results");

# Now start the keepalive thread to print something every few seconds
my $thr = threads->create(\&keepalive_thread);

if ($conf{'dbtype'} eq "hashdb") {
    foreach my $area (@chosen_areas) {
	my $db_file_name = "$conf{'dbdir'}/$area.db";
	$l .= "Looking in area $area, file $db_file_name<br>\n";
	dbmopen(%fileinfo, "$db_file_name", 0000) ||
	    log_error(500, "Failed to open db file: $!\n");

	if ($query_term =~ /[\*\?]/ || $query_type eq "simple") {
	    $using_glob = "(using globs)";
	    # Will need to search through all the keys to allow for glob
	    foreach my $file (keys %fileinfo) {
		if ($file =~ $re_search) {
		    $count++;
		    $count_images += scalar (split / /, $fileinfo{$file});
		    $results{$file} = $fileinfo{$file};
		    if ($count >= $max_count) {
			last;
		    }
		}
	    }
	} else {
	    # We've been given an exact name - do the exact key lookup \o/
	    if (defined($fileinfo{$query_term})) {
		$results{$query_term} = $fileinfo{$query_term};
		$count_images += scalar (split / /, $fileinfo{$query_term});
		$count++;
	    }
	}
	if ($count >= $max_count) {
	    last;
	}
	dbmclose %fileinfo;
    }
} else {
    foreach my $area (@chosen_areas) {
	my $sth;
	my @db_results;
	my $db_file_name = "$conf{'dbdir'}/$area.db.sqlite";
	my $dbh = DBI->connect("dbi:SQLite:dbname=$db_file_name","","", {
	    sqlite_open_flags => DBD::SQLite::OPEN_READONLY,
			       }) or log_error (500, "Failed to open DB file $db_file_name: $!\n");
	$dbh->do("PRAGMA synchronous = OFF");

	if ($query_term =~ /[\*\?]/ || $query_type eq "simple") {
	    $using_glob = "(using globs)";

	    # Will need to use sql LIKE and SQL wildcards
	    my $sql_term = "*" . $query_term . "*";
	    $sql_term =~ s,\*,\%,g;
	    $sql_term =~ s,\?,\_,g;
	    $sth = $dbh->prepare("SELECT * FROM entries WHERE filename LIKE ? ORDER BY filename ASC, jigdo ASC");
	    $sth->execute($sql_term);
	} else {
	    # We've been given an exact name - do the exact lookup
	    $sth = $dbh->prepare("SELECT * FROM entries WHERE filename=? ORDER BY filename ASC");
	    $sth->execute($query_term);
	}
	while (@db_results = $sth->fetchrow_array) {
	    my $file = $db_results[0];
	    my $image = $db_results[1];
	    if (defined($results{$file})) {
		$results{$file} = "$results{$file} $image";
		$count_images++;
	    } else {
		$results{$file} = "$image";
		$count++;
		$count_images++;
	    }
	    if ($count >= $max_count) {
		last;
	    }
	}
	undef $sth;
	$dbh->disconnect();
    }
}

# Kill the keepalive thread
$thr->kill('KILL')->detach();

my $end_time = time();
my $time_taken = $end_time - $start_time;

print
    $q->start_html("$title_base: $count results from $count_images images"),
    $q->h1($title_base), "\n";
    print_config_if_debug();
if ($conf{'debug'}) {
    print $q->h2("QUERY DEBUG"), "\n<ul>\n";
    print $q->li("areas: @chosen_areas");
    print $q->li("query type: $query_type");
    print $q->li("query term: \"$query_term\" $using_glob");
    print $q->li("re_search: \"$re_search\"");
    print $q->li("time taken: $time_taken sec\n");
    print "</ul>\n";
}
print $q->p("<a href=\"" . $q->url . "\">Search again.</a>");
if ($count >= $max_count) {
    print $q->p("More than $max_count results for $query_type search \"$query_term\". Showing the first $count only\n");
} else {   
    print $q->p("$count result(s), $count_images image(s) for \"$query_term\":\n");
}
if ($count > 0) {
    print "<ol>\n";
    foreach my $found (sort (keys %results)) {
	my @list = split(' ', $results{$found});
        print "<li> $found appears in:\n";
        print "<ul>\n";
        foreach my $image (sort(@list)) {
            print "<li>" . pretty_name($image);
            print " (<a href=\"" . list_link($image) . "\">list.gz</a> |";
            print " <a href=\"" . jigdo_link($image) . "\">jigdo</a> |";
            print " <a href=\"" . iso_link($image) . "\">iso</a>)\n";
        }
        print "</ul>\n";
    }
    print "</ol>\n";
}
print $q->p("<a href=\"" . $q->url . "\">Search again.</a>");
print_footer();
print $q->end_html;
