#!/usr/bin/perl
#
# find_file_db.pl
#
# Look through a provided database to find which CD/DVD image(s)
# contain a specified Debian package or source file.
# 
# Copyright (c) 2011-2024 Steve McIntyre <93sam@debian.org>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA

use strict;
use warnings;
use threads;
use DB_File;
use DBI;
use DBD::SQLite;

my $dbdir = "/home/steve/debian/debian-cd/search-db";
my @AREAS;
my %num_files;
my %fileinfo;
my %imageinfo;
my %results;
my $max_count = 1000;
my $dbtype = "sqlite";

# Borrowed from Ikiwiki.pm
sub glob2re ($) {
    my $re=quotemeta(shift);
    $re=~s/\\\*/.*/g;
    $re=~s/\\\?/./g;
    return qr/^$re$/i;
}

chdir($dbdir) || die "Failed to cd to $dbdir: $!\n";
opendir(my $dh, ".") || die "Failed to open $dbdir: $!\n";
while (defined($_ = readdir($dh))) {
    if ($dbtype eq "hash") {
	m/(.*)\.db$/ and push (@AREAS, $1);
    } else {
	m/(.*)\.db\.sqlite$/ and push (@AREAS, $1);
    }
}
closedir($dh);

my $query_term = shift;

if (!defined($query_term) || !length($query_term)) {
    die "No query term specified!\n";
}

sub keepalive_thread () {
    $SIG{'KILL'} = sub { threads->exit(); };
    while(1) {
	sleep 10;
	print "<!-- processing, please wait... ->\n"
    }
}

my $count = 0;
my $count_images = 0;
my $re_search = glob2re($query_term);
my $start_time = time();

# Now start the keepalive thread to print something every few seconds
#my $thr = threads->create(\&keepalive_thread);

if ($dbtype eq "hash") {
    foreach my $area (@AREAS) {
	print "Looking in area $area\n";
	my $db_file_name = "$dbdir/$area.db";
	dbmopen(%fileinfo, "$db_file_name", 0000) || 
	    die "Failed to open db file: $!\n";

	if ($query_term =~ /[\*\?]/) {
	    # Will need to search through all the keys to allow for glob
	    foreach my $file (keys %fileinfo) {
		if ($file =~ $re_search) {
		    $count++;
		    $count_images += scalar (split / /, $fileinfo{$file});
		    $results{$file} = $fileinfo{$file};
		    if ($count >= $max_count) {
			last;
		    }
		}
	    }
	} else {
	    # We've been given an exact name - do the exact key lookup \o/
	    if (defined($fileinfo{$query_term})) {
		$results{$query_term} = $fileinfo{$query_term};
		$count_images += scalar (split / /, $fileinfo{$query_term});
		$count++;
	    }
	}
	if ($count >= $max_count) {
	    last;
	}
	dbmclose %fileinfo;
    }
} else {
    foreach my $area (@AREAS) {
	print "Looking in area $area\n";
	my $sth;
	my @db_results;
	my $db_file_name = "$dbdir/$area.db.sqlite";
	my $dbh = DBI->connect("dbi:SQLite:dbname=$db_file_name","","", {
	    sqlite_open_flags => DBD::SQLite::OPEN_READONLY,
			       }) or die "Failed to open DB file $db_file_name: $!\n";
	$dbh->do("PRAGMA synchronous = OFF");

	if ($query_term =~ /[\*\?]/) {
	    # Will need to use sql LIKE and SQL wildcards
	    my $sql_term = $query_term;
	    $sql_term =~ s,\*,\%,g;
	    $sql_term =~ s,\?,\_,g;
	    $sth = $dbh->prepare("SELECT * FROM entries WHERE filename LIKE ? ORDER BY filename ASC, jigdo ASC");
	    $sth->execute($sql_term);
	} else {
	    # We've been given an exact name - do the exact lookup
	    $sth = $dbh->prepare("SELECT * FROM entries WHERE filename=? ORDER BY filename ASC");
	    $sth->execute($query_term);
	}
	while (@db_results = $sth->fetchrow_array) {
	    my $file = $db_results[0];
	    my $image = $db_results[1];
	    if (defined($results{$file})) {
		$results{$file} = "$results{$file} $image";
		$count_images++;
	    } else {
		$results{$file} = "$image";
		$count++;
		$count_images++;
	    }
	    #			print "@results\n";
	    if ($count >= $max_count) {
		last;
	    }
	}
	undef $sth;
	$dbh->disconnect();
    }
}
# Kill the keepalive thread
#$thr->kill('KILL')->join();

my $end_time = time();
my $time_taken = $end_time - $start_time;

print "Using $dbtype database, took $time_taken seconds:\n";
if ($count >= $max_count) {
    print "More than $max_count results for \"$query_term\", showing the first $count only\n";
} else {   
    print "$count result(s), $count_images image(s) for \"$query_term\":\n";
}

foreach my $found (sort (keys %results)) {
    my @list = split(' ', $results{$found});
    print "  $found:\n";
    foreach my $image (sort(@list)) {
	print "    $image\n";
    }
}

