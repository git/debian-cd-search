#!/usr/bin/perl -w
#
# update-lists-db.pl
#
# Parse the list.gz files that are created on cdimage.debian.org and
# convert the contents into a database for lookup tools to use.
# 
# Copyright (c) 2011-2024 Steve McIntyre <93sam@debian.org>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA

use strict;
use Fcntl qw(:flock SEEK_END);
use File::stat;
use File::Find;
use File::Basename;
use Compress::Zlib;
use POSIX qw(ENOENT EROFS ENOSYS EEXIST EPERM EBUSY O_RDONLY O_RDWR O_APPEND O_CREAT);
use Fcntl qw(O_RDWR O_WRONLY);
use DB_File;
use DBI;
use Getopt::Long;

# Set defaults for these
my $dbdir = "/home/steve/debian/debian-cd/search-db";
my $treedir = "/home/steve/debian/debian-cd/jigdo-lists";
my $dbtype = "sqlite";
my $verbose = 0;

GetOptions ("dbdir=s"     => \$dbdir,      # numeric
	    "treedir=s"   => \$treedir,    # string
	    "dbtype=s"    => \$dbtype,     # string
	    "verbose=i"   => \$verbose)    # numeric
    or die("Error in command line arguments\n");

if (!($dbtype eq "hashdb" or $dbtype eq "sqlite")) {
    die "Invalid dbtype $dbtype\n";
}

my $lock = "$dbdir/.update.lock";
my @areas = qw(daily-builds release weekly-builds archive);
#my @areas = qw(daily-builds release weekly-builds);
#my @areas = qw(weekly-builds);
my $update_needed = 0;
my $num_list_files;
my $db_mtime = 0;
my $db_file_name;
my $list_file_name;
my $list_file;
my $dbh;
my $sth;
my ($start, $stop, $taken);

sub get_time()
{
    my @tm;
    my $text;

    @tm = gmtime();
    $text = sprintf("%4d-%02d-%02d %02d:%02d:%02d UTC",
                    (1900 + $tm[5]),(1 + $tm[4]),$tm[3],$tm[2],$tm[1],$tm[0]);
    return $text;
}

sub print_log {
    my $level = shift;
    my $msg = shift;
    if ($level <= $verbose) {
		my $timestamp = get_time();
		print "$timestamp $msg";
    }
}

sub lock {
    my ($fh) = @_;
    flock($fh, LOCK_EX) or die "Cannot lock lockfile - $!\n";
    
    # and, in case someone appended while we were waiting...
    seek($fh, 0, SEEK_END) or die "Cannot seek - $!\n";
}

sub unlock {
    my ($fh) = @_;
    flock($fh, LOCK_UN) or die "Cannot unlock lockfile - $!\n";
}

sub file_mtime {
    my ($file) = shift;
    my $sb = lstat($file);
    if (! -e $file) {
		print "ENOENT $file!\n";
    }
    return $sb->mtime;
}

sub check_newer {
    my ($filename);
    $filename = $File::Find::name;
    if ($filename =~ m/\.list\.gz$/) {
		my $mtime = file_mtime("/$treedir/$filename");
		print_log(4, "  check_newer: found $filename\n");
		print LISTS "$filename\n";
		if ($mtime > $db_mtime) {
			$update_needed = 1;
		}
		$num_list_files++;
		if (!($num_list_files % 1000)) {
			print_log(3, "  check_newer: found $num_list_files list files\n");
		}
    }
}

chdir "$treedir";

open(my $lockfile, ">>", "$lock") or die "Can't open lockfile: $!";
print_log(1, "waiting on lock for $lock\n");
lock($lockfile);
print_log(1, "lock acquired for $lock\n");

foreach my $area (@areas) {

	if ($dbtype =~ "hash") {
		$update_needed = 0;
		$num_list_files = 0;
		$db_file_name = "$dbdir/$area.db";
		$list_file_name = "$dbdir/$area.lists";

		print_log(1, "Working on area $area:\n");
		unlink "$list_file_name.new", "$list_file_name";
		open(LISTS, ">> $list_file_name.new") or die ("Can't open lists file $list_file_name.new for writing: $!\n");
		if (-f $db_file_name) {
			$db_mtime = file_mtime($db_file_name);
		} else {
			$db_mtime = 0;
		}
		find (\&check_newer,  "$area");
		close LISTS;
		print_log(2, "  found $num_list_files list files total, update_needed $update_needed\n");

		if ($update_needed) {
			my $current_list_num = 0;
			my $num_files = 0;
			my $current_file = 0;
			my $unique_files = 0;
			my %fileinfo;
			my %dbinfo;

			# Two passes; work in memory first, then push to the DB
			# file. Will this work better?
			undef %fileinfo;
			undef %dbinfo;

			$start = time();
			unlink "$db_file_name.new";
			open(LISTS, "< $list_file_name.new") or die ("Can't open lists file $list_file_name.new for reading: $!\n");
			while (my $listfile = <LISTS>) {
				$current_list_num++;
				chomp $listfile;
				my $gz = gzopen($listfile, "rb") or die "Cannot open $listfile: $gzerrno\n";
				my $file;
				while ($gz->gzreadline($file) > 0) {
					chomp $file;
					$num_files++;
					if (defined($fileinfo{$file})) {
						$fileinfo{$file} = "$fileinfo{$file} $listfile";
					} else {
						$fileinfo{$file} = "$listfile";
						$unique_files++;
					}
				}
				$gz->gzclose();
				if (!($current_list_num % 100)) {
					print_log(3, "    processing $area in memory: $current_list_num/$num_list_files list files done, $num_files files ($unique_files unique)\n");
				}
			}

			# now push to the hashdb
			tie %dbinfo, 'DB_File', "$db_file_name.new";
			foreach my $file (keys %fileinfo) {
				$dbinfo{$file} = $fileinfo{$file};	    
				$current_file++;
				if (!($current_file % 10000)) {
					print_log(3, "    storing $area to hashdb: $current_file/$unique_files files added\n");
				}
			}
			untie %dbinfo;
			$stop = time();
			$taken = $stop - $start;
			
			rename("$db_file_name.new", "$db_file_name");
			print_log(2, "  $db_file_name created in $taken sec: $num_list_files list files, $num_files files referenced\n");
		}
	} elsif ($dbtype =~ "sqlite") {
		$update_needed = 0;
		$num_list_files = 0;
		$db_file_name = "$dbdir/$area.db.sqlite";
		$list_file_name = "$dbdir/$area.lists";

		print_log(1, "Working on area $area:\n");
		unlink "$list_file_name.new", "$list_file_name";
		open(LISTS, ">> $list_file_name.new") or die ("Can't open lists file $list_file_name.new for writing: $!\n");
		if (-f $db_file_name) {
			$db_mtime = file_mtime($db_file_name);
		} else {
			$db_mtime = 0;
		}
		find (\&check_newer,  "$area");
		close LISTS;
		print_log(2, "  found $num_list_files list files total, update_needed $update_needed\n");

		if ($update_needed) {
			my $current_list_num = 0;
			my $num_files = 0;
			my $current_file = 0;

			unlink "$db_file_name.new";
			$start = time();
			$dbh = DBI->connect("dbi:SQLite:dbname=$db_file_name.new","","");

			# Maximise performance - we're doing a bulk insert for a cache...
			$dbh->do("PRAGMA synchronous = OFF");
			$dbh->do("PRAGMA journal_mode = MEMORY");
			$dbh->do("PRAGMA locking_mode = EXCLUSIVE");
			$sth = $dbh->prepare("CREATE TABLE entries (filename VARCHAR(512), jigdo VARCHAR(512));");
			$sth->execute();

			$dbh->do("BEGIN TRANSACTION");
			open(LISTS, "< $list_file_name.new") or die ("Can't open lists file $list_file_name.new for reading: $!\n");
			$sth = $dbh->prepare("INSERT INTO entries VALUES (?, ?)");
			while (my $listfile = <LISTS>) {
				$current_list_num++;
				chomp $listfile;
				my $gz = gzopen($listfile, "rb") or die "Cannot open $listfile: $gzerrno\n";
				my $file;
				while ($gz->gzreadline($file) > 0) {
					chomp $file;
					$sth->execute($file, $listfile);
					$num_files++;
				}
				$gz->gzclose();
				if (!($current_list_num % 100)) {
					print_log(3, "    processing $area into sqlite: $current_list_num/$num_list_files list files done, $num_files files\n");
				}
			}
			if ($current_list_num % 100) {
				print_log(3, "    finished processing $area into sqlite: $current_list_num/$num_list_files list files done, $num_files files; creating index now\n");
			}
			$dbh->do("CREATE INDEX fn_index ON entries (filename);");
			$dbh->do("END TRANSACTION");
			$dbh->disconnect();
			$stop = time();
			$taken = $stop - $start;
			rename("$db_file_name.new", "$db_file_name");
			print_log(2, "  $db_file_name created in $taken sec: $num_list_files list files, $num_files files referenced\n");
		}
	}
}
rename("$list_file_name.new", "$list_file_name");
print_log(1, "dropping lock for $lock\n");
unlock($lockfile);
close($lockfile);
